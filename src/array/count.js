module.exports = function(arg) {
    if (typeof arg === 'function') {
        return this.filter(arg).length;
    }
    return this.filter(val => val === arg).length;
};
