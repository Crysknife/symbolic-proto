module.exports = function() {
    return !Object.getOwnPropertyNames(this).some(key =>
        ({}.hasOwnProperty.call(this, key))
    );
};
