module.exports = function() {
    return !Object.keys(this).some(key => ({}.hasOwnProperty.call(this, key)));
};
