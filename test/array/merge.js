const { assert } = require('chai');
const { merge } = symbols;

module.exports = () => {
    describe('[merge]()', () => {
        it('merges target array into subject', () => {
            const array = [1, 2, 3];
            const mergedArray = array[merge]([4, 5, 6]);

            assert.sameOrderedMembers(mergedArray, [1, 2, 3, 4, 5, 6]);
        });

        it('mutates the original array', () => {
            const array = [1, 2, 3];
            array[merge]([4, 5, 6]);

            assert.sameOrderedMembers(array, [1, 2, 3, 4, 5, 6]);
        });

        it('simply pushes an args, if it\'s not an array', () => {
            const array = [1, 2, 3];
            const mergedArray = array[merge]('hello');

            assert.sameOrderedMembers(mergedArray, [1, 2, 3, 'hello']);
        });
    });
};
