const snakefy = require('../string/snakefy');
const symbols = require('../symbols');
const propNames = [
    'floor',
    'ceil',
    'round',
    'times',
    'in',
    'between',
    'pad',
    'roundTo',
    'isEven',
    'isOdd'
];
const props = {};
propNames.forEach(prop => {
    props[symbols[prop]] = { value: require(`./${snakefy.call(prop)}`) };
});

Object.defineProperties(Number.prototype, props);
