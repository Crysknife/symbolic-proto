module.exports = function(len) {
    return `${this.substr(0, len).trimEnd()}...`;
};
