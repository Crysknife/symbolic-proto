const snakefy = require('../string/snakefy');
const symbols = require('../symbols');
const propNames = [
    'isObject',
    'isCompletelyEmpty',
    'isEmpty',
    'deepCopy',
    'clear',
    'isEqual',
];
const props = {};
propNames.forEach(prop => {
    props[symbols[prop]] = { value: require(`./${snakefy.call(prop)}`) };
});

Object.defineProperties(Object.prototype, props);
