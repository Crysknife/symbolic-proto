[
    // number
    'floor',
    'ceil',
    'round',
    'times',
    'in',
    'between',
    'pad',
    'roundTo',
    'isEven',
    'isOdd',
    // string
    'truncate',
    // array
    'last',
    'remove',
    'count',
    'merge',
    'purge',
    // object
    'isObject',
    'isCompletelyEmpty',
    'isEmpty',
    'deepCopy',
    // object + array
    'clear',
    'isEqual',
].forEach(symbol => module.exports[symbol] = Symbol(symbol));
