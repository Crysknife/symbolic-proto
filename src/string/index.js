const snakefy = require('../string/snakefy');
const symbols = require('../symbols');
const propNames = [
    'snakefy',
    'truncate',
];
const props = {};
propNames.forEach(prop => {
    props[symbols[prop]] = { value: require(`./${snakefy.call(prop)}`) };
});

Object.defineProperties(String.prototype, props);
