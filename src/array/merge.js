module.exports = function(...args) {
    args.forEach(arg => {
        if (Array.isArray(arg)) {
            arg.forEach(item => {
                this.push(item);
            });
        } else {
            this.push(arg);
        }
    });
    return this;
};
