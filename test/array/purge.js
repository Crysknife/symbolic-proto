const { assert } = require('chai');
const { purge } = symbols;

module.exports = () => {
    describe('[purge]()', () => {
        it('purges target array', () => {
            const array = [1, 2, 3];
            const purgedArray = array[purge]();

            assert.isEmpty(purgedArray, []);
        });

        it('mutates the original array', () => {
            const array = [1, 2, 3];
            array[purge]();

            assert.isEmpty(array, []);
        });
    });
};
