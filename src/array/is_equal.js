/* eslint-disable eqeqeq */

module.exports = function(arr, notStrictly = false) {
    if (!Array.isArray(arr)) {
        throw new Error('First argument should be an array');
    }
    if (this.length !== arr.length) return false;

    return this.reduce((acc, cur, ind) => {
        if (!acc) return acc;
        return notStrictly ? cur == arr[ind] : cur === arr[ind];
    }, true);
};
