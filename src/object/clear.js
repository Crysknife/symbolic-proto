module.exports = function() {
    return Object.keys(this).reduce((acc, key) => {
        if (this[key] !== undefined && this[key] !== null) acc[key] = this[key];
        return acc;
    }, {});
};
