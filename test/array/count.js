const { assert } = require('chai');
const { count } = symbols;

module.exports = () => {
    describe('[count]()', () => {
        it('counts array items that are equal to arg', () => {
            const array = [1, 2, 2, 2, 1, 4];
            const countTwos = array[count](2);
            const countOnes = array[count](1);

            assert.strictEqual(countTwos, 3);
            assert.strictEqual(countOnes, 2);
        });

        it('uses callback as filter, if arg is a callback', () => {
            const array = [0, 1, 2, 3, 4, 5, 6];
            const countEven = array[count](num => num % 2 === 0);
            const countOdd = array[count](num => num % 2 !== 0);

            assert.strictEqual(countEven, 4);
            assert.strictEqual(countOdd, 3);
        });
    });
};
