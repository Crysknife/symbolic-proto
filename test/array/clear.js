const { assert } = require('chai');
const { clear } = symbols;

module.exports = () => {
    describe('[clear]()', () => {
        it('returns a new array without nulls and undefined', () => {
            const array = [1, undefined, 3, null, 'hello'];
            const clearedArray = array[clear]();

            assert.sameOrderedMembers(clearedArray, [1, 3, 'hello']);
            assert.sameOrderedMembers(array, [1, undefined, 3, null, 'hello']);
        });

        it('mutates target array, if respective arg is specified', () => {
            const array = [1, undefined, 3, null, 'hello'];
            const clearedArray = array[clear](true);

            assert.sameOrderedMembers(clearedArray, [1, 3, 'hello']);
            assert.sameOrderedMembers(array, [1, 3, 'hello']);
        });
    });
};
