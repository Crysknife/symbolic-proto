const count = require('./count');
const merge = require('./merge');
const clear = require('./clear');
const isEqual = require('./is_equal');
const purge = require('./purge');

module.exports = () => {
    describe('Array', () => {
        count();
        merge();
        clear();
        isEqual();
        purge();
    });
};
