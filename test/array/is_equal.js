const { assert } = require('chai');
const { isEqual } = symbols;

module.exports = () => {
    describe('[isEqual]()', () => {
        it('compares array with target array', () => {
            const array = [1, 2, 3];
            const sameArray = [1, 2, 3];
            const notSameArray = [1, 2, 4];
            const arrayIsEqualSameArray = array[isEqual](sameArray);
            const arrayIsNotEqualNotSameArray = array[isEqual](notSameArray);

            assert.isTrue(arrayIsEqualSameArray);
            assert.isNotTrue(arrayIsNotEqualNotSameArray);
        });

        it('compares arrays\' items not strictly with respective param', () => {
            const array = [1, 2, 3];
            const sameArray = [1, 2, '3'];
            const notSameArray = [1, 2, '4'];
            const arrayIsEqualSameArray = array[isEqual](sameArray, true);
            const arrayIsNotEqualNotSameArray = array[isEqual](notSameArray, true);

            assert.isTrue(arrayIsEqualSameArray);
            assert.isNotTrue(arrayIsNotEqualNotSameArray);
        });
    });
};
