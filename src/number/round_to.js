module.exports = function(order) {
    let multiplier = Math.pow(10, order);
    return Math.round(this * multiplier) / multiplier;
};
