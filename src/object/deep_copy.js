const isObject = require('./is_object');

module.exports = function() {
    // const seenObjects = [this];
    // const circular = '{ Circular }';
    const copyLevel = function() {
        let copy = { ...this };
        Object.keys(copy).forEach(key => {
            let prop = this[key];
            if (isObject.call(prop)) {
                this[key] = copyLevel.call(prop);
            }
        });
        return copy;
    };
    return copyLevel.call(this);
};
