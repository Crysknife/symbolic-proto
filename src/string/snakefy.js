module.exports = function() {
    let string = this.replace(/([a-z])([A-Z](?![A-Z]+))/g, (match, p1, p2) => {
        return `${p1}_${p2.toLowerCase()}`;
    });
    if (string.search(/[A-Z]+/)) string = string.replace(/([A-Z]+)/g, '_$1');
    return string;
};
