const snakefy = require('../string/snakefy');
const symbols = require('../symbols');
const propNames = [
    'last',
    'remove',
    'count',
    'merge',
    'purge',
    'clear',
    'isEqual',
];
const props = {};
propNames.forEach(prop => {
    props[symbols[prop]] = { value: require(`./${snakefy.call(prop)}`) };
});

Object.defineProperties(Array.prototype, props);
