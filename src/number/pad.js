module.exports = function(length = 2, filling = 0) {
    return (`${this}`).padStart(length, filling);
};
