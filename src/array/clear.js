module.exports = function(mutate = false) {
    let undefinedOrNull = item => (item === undefined || item === null);

    if (mutate) {
        while (this.some(undefinedOrNull)) {
            let index = this.findIndex(undefinedOrNull);
            this.splice(index, 1);
        }
        return this;
    }

    return this.filter(item => !undefinedOrNull(item));
};
