module.exports = function(min, max) {
    return this > min && this < max;
};
