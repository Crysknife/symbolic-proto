module.exports = function(cb) {
    const array = [];
    for (let i = 0; i < this; i++) {
        if (typeof cb === 'function') array.push(cb(i));
        if (cb === true) array.push(i + 1);
        if (!cb) array.push(i);
    }
    return array;
};
