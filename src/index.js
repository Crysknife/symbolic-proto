require('./number');
require('./string');
require('./array');
require('./object');

const symbols = require('./symbols');
module.exports = symbols;
